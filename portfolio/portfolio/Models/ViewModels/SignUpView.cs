﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace portfolio.Models.ViewModels {
	public class SignUpView {
		[Required]
		[Display(Name = "Username")]
		[MaxLength(64)]
		[DataType(DataType.Text)]
		public string Username { get; set; }

		[Required]
		[Display(Name = "Password")]
		[MaxLength(256)]
		[DataType(DataType.Password)]
		public string Password { get; set; }

		[Required]
		[Display(Name = "Confirm password")]
		[MaxLength(256)]
		[DataType(DataType.Password)]
		public string ConfirmPassword { get; set; }

		[Required]
		[Display(Name = "Email")]
		[MaxLength(64)]
		[RegularExpression("^[a-zA-Z0-9_]{1,}@[a-zA-Z]{1,}[.]{1}[a-zA-Z]{1,}$")]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }

		[Required]
		[Display(Name = "Firstname")]
		[MaxLength(64)]
		[DataType(DataType.Text)]
		public string Firstname { get; set; }

		[Required]
		[Display(Name = "Lastname")]
		[MaxLength(64)]
		[DataType(DataType.Text)]
		public string Lastname { get; set; }
	}
}