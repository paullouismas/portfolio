﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace portfolio.Models.ViewModels {
	public class SaveRole {
		[Required]
		public int IdUser { get; set; }

		[Required]
		public int IdRole { get; set; }
	}
}