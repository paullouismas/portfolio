﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace portfolio.Models.ViewModels {
	public class LoginView {
		[Required]
		[Display(Name = "Username")]
		[MaxLength(64)]
		[DataType(DataType.Text)]
		public string Username { get; set; }

		[Required]
		[Display(Name = "Password")]
		[MaxLength(256)]
		[DataType(DataType.Password)]
		public string Password { get; set; }

		[Required]
		[Display(Name = "Stay connected")]
		[DataType(DataType.Custom)]
		public bool Persistant { get; set; } = false;
	}
}