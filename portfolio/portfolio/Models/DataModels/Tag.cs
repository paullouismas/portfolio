﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace portfolio.Models.DataModels {
	public class Tag {
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Index(IsUnique = true)]
		public int IdTag { get; set; }

		[Required]
		[Display(Name = "Tag name")]
		[Index(IsUnique = true)]
		[MaxLength(64)]
		[DataType(DataType.Text)]
		public string Name { get; set; }
	}
}