﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Security;

namespace portfolio.Models.DataModels {
	public class User {
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Index(IsUnique = true)]
		public int IdUser { get; set; }

		[Required]
		[Display(Name = "Username")]
		[Index(IsUnique = true)]
		[MaxLength(64)]
		[DataType(DataType.Text)]
		public string Username { get; set; }

		[Required]
		[Display(Name = "Password")]
		[Index(IsUnique = false)]
		[MaxLength(256)]
		[DataType(DataType.Password)]
		public string Password { get; set; }

		[Required]
		[Display(Name = "Email")]
		[Index(IsUnique = true)]
		[MaxLength(64)]
		[RegularExpression("^[a-zA-Z0-9_]{1,}@[a-zA-Z]{1,}[.]{1}[a-zA-Z]{1,}$")]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }

		[Required]
		[Display(Name = "Firstname")]
		[Index(IsUnique = false)]
		[MaxLength(64)]
		[DataType(DataType.Text)]
		public string Firstname { get; set; }

		[Required]
		[Display(Name = "Lastname")]
		[Index(IsUnique = false)]
		[MaxLength(64)]
		[DataType(DataType.Text)]
		public string Lastname { get; set; }

		[ForeignKey("Roles")]
		public int IdUserRole { get; set; }

		[Required]
		[Display(Name = "Role")]
		[DataType(DataType.Custom)]
		[ForeignKey("IdUserRole")]
		[InverseProperty("Users")]
		public virtual ICollection<UserRole> Roles { get; set; } = new HashSet<UserRole>();

		[Display(Name = "Fullname")]
		public string Fullname { get => $"{this.Firstname} {this.Lastname}"; }

		public static string HashPassword(string plainPassword, byte[] salt = null) {
			if (salt == null) {
				new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
			}

			Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(plainPassword, salt, 10000);

			byte[] hash = pbkdf2.GetBytes(20);
			byte[] hashBytes = new byte[36];

			Array.Copy(salt, 0, hashBytes, 0, 16);
			Array.Copy(hash, 0, hashBytes, 16, 20);

			string hashedPassword = Convert.ToBase64String(hashBytes);

			return hashedPassword;
		}

		public bool VerifyPassword(string plainPassword) {
			string hashedPassword = this.Password;
			byte[] hashBytes = Convert.FromBase64String(hashedPassword);
			byte[] salt = new byte[16];

			Array.Copy(hashBytes, 0, salt, 0, 16);

			string hash = HashPassword(plainPassword, salt);

			return this.Password == hash;
		}
	}

	public class UserRoleProvider : RoleProvider {
		private readonly PortfolioDB db = new PortfolioDB();

		public override string[] GetRolesForUser(string username) {
			User user = this.db.Users.SingleOrDefault(u => u.Username == username);

			string[] roles = user.Roles.Select(role => role.Shortname).ToArray();

			return roles;
		}
		
		public override bool IsUserInRole(string username, string roleName) {
			return this.GetRolesForUser(username).Contains(roleName);
		}
		
		public override string[] GetAllRoles() {
			string[] roles = this.db.Roles.Select(role => role.Shortname).ToArray();

			return roles;
		}
		
		public override string[] GetUsersInRole(string roleName) {
			string[] usersInRole = this.db.Users.Where(user => user.Roles.Any(role => role.Shortname == roleName)).Select(user => user.Username).ToArray();

			return usersInRole;
		}
		
		public override void AddUsersToRoles(string[] usernames, string[] roleNames) {
			IEnumerable<User> users = this.db.Users.Where(user => usernames.Contains(user.Username)).ToList();
			IEnumerable<UserRole> roles = this.db.Roles.Where(role => roleNames.Contains(role.Shortname)).ToList();

			foreach (User user in users) {
				foreach (UserRole role in roles) {
					user.Roles.Add(role);
				}
			}
		}
		
		public override void CreateRole(string roleName) => throw new NotImplementedException();
		
		public override bool DeleteRole(string roleName, bool throwOnPopulatedRole) => throw new NotImplementedException();
		
		public override string[] FindUsersInRole(string roleName, string usernameToMatch) => throw new NotImplementedException();
		
		public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames) {
			IEnumerable<User> users = this.db.Users.Where(user => usernames.Contains(user.Username)).ToList();
			IEnumerable<UserRole> roles = this.db.Roles.Where(role => roleNames.Contains(role.Shortname)).ToList();

			foreach (User user in users) {
				foreach (UserRole role in roles) {
					user.Roles.Remove(role);
				}
			}
		}
		
		public override bool RoleExists(string roleName) {
			bool exist = this.db.Roles.Any(role => role.Shortname == roleName);

			return exist;
		}

		public override string ApplicationName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
	}
}