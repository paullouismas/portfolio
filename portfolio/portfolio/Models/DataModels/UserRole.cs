﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace portfolio.Models.DataModels {
	public class UserRole {
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Index(IsUnique = true)]
		public int IdUserRole { get; set; }

		[Required]
		[Display(Name = "Role name")]
		[Index(IsUnique = true)]
		[MaxLength(64)]
		[DataType(DataType.Text)]
		public string Name { get; set; }

		[Required]
		[Display(Name = "Role short name")]
		[Index(IsUnique = true)]
		[MaxLength(64)]
		[DataType(DataType.Text)]
		public string Shortname { get; set; }

		[ForeignKey("Users")]
		public virtual int IdUser { get; set; }

		[ForeignKey("IdUser")]
		[InverseProperty("Roles")]
		public virtual ICollection<User> Users { get; set; } = new HashSet<User>();
	}
}