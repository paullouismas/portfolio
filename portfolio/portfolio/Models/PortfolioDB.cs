﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using portfolio.Models.DataModels;

namespace portfolio.Models {
	public class PortfolioDB : DbContext {
		public PortfolioDB() : base("PortfolioDBConnectionString") {
			Database.SetInitializer(new PortfolioDBInitializer());

			this.Configuration.LazyLoadingEnabled = true;
		}

		public DbSet<Tag> Tags { get; set; }
		public DbSet<User> Users { get; set; }
		public DbSet<UserRole> Roles { get; set; }
	}
}