﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using portfolio.Models.DataModels;

namespace portfolio.Models {
	public class PortfolioDBInitializer : DropCreateDatabaseIfModelChanges<PortfolioDB> {
		protected override void Seed(PortfolioDB context) {
			UserRole role_admin = new UserRole { Name = "Administrator", Shortname = "admin" };
			UserRole role_member = new UserRole { Name = "Member", Shortname = "member" };

			User user_admin = new User {
				Email = "admin@admin.com", 
				Firstname = "admin", 
				Lastname = "admin", 
				Password = "admin", 
				Roles = new HashSet<UserRole> { role_member, role_admin }, 
				Username = "admin"
			};

			context.Roles.Add(role_admin);
			context.Roles.Add(role_member);

			context.Users.Add(user_admin);

			context.SaveChanges();
		}
	}
}