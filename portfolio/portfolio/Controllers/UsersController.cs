﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using portfolio.Models;
using portfolio.Models.DataModels;
using portfolio.Models.ViewModels;

namespace portfolio.Controllers {
	[Authorize]
	public class UsersController: Controller {// #TODO: complete the logic to identify admins from regular users
		private readonly PortfolioDB db = new PortfolioDB();

		[HttpGet]
		[Authorize]
		public ActionResult Index() {
			IEnumerable<User> users = this.db.Users.ToList();

			return View(users);
		}

		[HttpGet]
		[Authorize]
		public ActionResult Details() {
			User user = this.db.Users.Find(int.Parse(User.Identity.Name));

			if (user == null) {
				return HttpNotFound();
			}

			return View(user);
		}

		[HttpGet]
		[AllowAnonymous]
		public ActionResult Signup() {
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult Signup(SignUpView signupUser) {
			if (ModelState.IsValid) {
				if (signupUser.Password != signupUser.ConfirmPassword) {
					ModelState.AddModelError("", "Passwords does not match");

					return View(signupUser);
				}

				UserRole role_member = this.db.Roles.Single(role => role.Shortname == "member");

				User user = new User {
					Email = signupUser.Email, 
					Firstname = signupUser.Firstname, 
					Lastname = signupUser.Lastname, 
					Password = signupUser.Password, 
					Username = signupUser.Username, 
					Roles = new HashSet<UserRole> { role_member }
				};

				this.db.Users.Add(user);
				
				this.db.SaveChanges();
				
				return RedirectToAction("Index");
			}

			return View(signupUser);
		}

		[HttpGet]
		[Authorize]
		public ActionResult Edit() {
			User user = this.db.Users.Find(int.Parse(User.Identity.Name));
			
			if (user == null) {
				return HttpNotFound();
			}

			return View(user);
		}

		[HttpPost]
		[Authorize]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id, Username, Password, Email, Firstname, Lastname")] User user) {
			if (ModelState.IsValid) {
				if (this.db.Users.Find(int.Parse(User.Identity.Name)).IdUser != user.IdUser) {
					return HttpNotFound();
				}

				this.db.Entry(user).State = EntityState.Modified;
				
				this.db.SaveChanges();

				return RedirectToAction("Index");
			}

			return View(user);
		}

		[HttpGet]
		[Authorize]
		public ActionResult Delete() {
			User user = this.db.Users.Find(int.Parse(User.Identity.Name));

			if (user == null) {
				return HttpNotFound();
			}

			return View(user);
		}

		[HttpPost]
		[Authorize]
		[ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed() {
			User user = this.db.Users.Find(int.Parse(User.Identity.Name));

			this.db.Users.Remove(user);

			this.db.SaveChanges();

			return RedirectToAction("Index");
		}

		[HttpGet]
		[Authorize]
		public ActionResult SaveRole() {
			return RedirectToAction("Index");
		}

		[HttpPost]
		[Authorize]
		public ActionResult SaveRole(SaveRole saveRole) {
			if (ModelState.IsValid) {
				User targetUser = this.db.Users.Find(saveRole.IdUser);
				UserRole targetRole = this.db.Roles.Find(saveRole.IdRole);

				targetUser.Roles.Add(targetRole);

				this.db.Entry(targetUser).State = EntityState.Modified;

				this.db.SaveChanges();
			}

			return RedirectToAction("Index");
		}

		[HttpGet]
		[AllowAnonymous]
		public ActionResult Login() {
			if (User.Identity.IsAuthenticated) {
				return RedirectToAction("Index");
			}

			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		public ActionResult Login(LoginView loginUser, string returnUrl = "/") {
			if (ModelState.IsValid) {
				User user = this.db.Users.SingleOrDefault(u => u.Username == loginUser.Username);

				if (user == null || !user.VerifyPassword(loginUser.Password)) {
					return View(loginUser);
				}

				FormsAuthentication.SetAuthCookie(user.Username, loginUser.Persistant);

				return Redirect(returnUrl);
			}

			return View(loginUser);
		}

		[HttpPost]
		[Authorize]
		public ActionResult Logout() {
			FormsAuthentication.SignOut();

			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing) {
			if (disposing) {
				this.db.Dispose();
			}

			base.Dispose(disposing);
		}
	}
}
