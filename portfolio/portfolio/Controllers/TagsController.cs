﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using portfolio.Models;
using portfolio.Models.DataModels;

namespace portfolio.Controllers {
	[Authorize]
	public class TagsController: Controller { // #TODO: complete the logic in the controller
		private readonly PortfolioDB db = new PortfolioDB();

		[HttpGet]
		[AllowAnonymous]
		public ActionResult Index() {
			IEnumerable<Tag> tags = this.db.Tags.ToList();

			return View(tags);
		}

		[HttpGet]
		[AllowAnonymous]
		public ActionResult Details(int? TagId) {
			if (TagId == null) {
				return RedirectToAction("Index");
			}

			Tag tag = this.db.Tags.Find(TagId);

			if (tag == null) {
				return HttpNotFound();
			}

			return View(tag);
		}

		[HttpGet]
		[Authorize]
		public ActionResult Create() {
			return View();
		}

		[HttpPost]
		[Authorize]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "Id, Name")] Tag tag) {
			if (ModelState.IsValid) {
				this.db.Tags.Add(tag);
				
				this.db.SaveChanges();

				return RedirectToAction("Index");
			}

			return View(tag);
		}

		[HttpGet]
		[Authorize]
		public ActionResult Edit(int? TagId) {
			if (TagId == null) {
				return RedirectToAction("Index");
			}

			Tag tag = this.db.Tags.Find(TagId);

			if (tag == null) {
				return HttpNotFound();
			}

			return View(tag);
		}

		[HttpPost]
		[Authorize]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id, Name")] Tag tag) {
			if (ModelState.IsValid) {
				this.db.Entry(tag).State = EntityState.Modified;

				this.db.SaveChanges();

				return RedirectToAction("Index");
			}
			return View(tag);
		}

		[HttpGet]
		[Authorize]
		public ActionResult Delete(int? TagId) {
			if (TagId == null) {
				return RedirectToAction("Index");
			}

			Tag tag = this.db.Tags.Find(TagId);

			if (tag == null) {
				return HttpNotFound();
			}

			return View(tag);
		}

		[HttpPost]
		[Authorize]
		[ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int TagId) {
			Tag tag = this.db.Tags.Find(TagId);

			this.db.Tags.Remove(tag);

			this.db.SaveChanges();

			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing) {
			if (disposing) {
				this.db.Dispose();
			}

			base.Dispose(disposing);
		}
	}
}
