﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using portfolio.Models;

namespace portfolio.Controllers {
	[Authorize]
	public class PortfolioController: Controller {
		private readonly PortfolioDB db = new PortfolioDB();

		[HttpGet]
		[AllowAnonymous]
		public ActionResult Index() {
			return View();
		}
	}
}